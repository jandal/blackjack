import java.util.ArrayList;
import java.util.Random;

public class Deck {
    static ArrayList<Card> deck;
    static Random random = new Random();

    public Deck() {
        buildDeck();
        shuffleDeck();
    }

    public void buildDeck() {
        deck = new ArrayList<>();
        String[] values = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
        String[] types = {"C", "D", "H", "S"};

        for (String type : types) {
            for (String value : values) {
                Card card = new Card(value, type);
                deck.add(card);
            }
        }
    }

    public Card getCard() {
        Card card = deck.remove(deck.size() - 1);
        return card;
    }

    public void shuffleDeck() {
        for (int i = 0; i < deck.size(); i++) {
            int j = random.nextInt(deck.size());
            Card currCard = deck.get(i);
            Card randomCard = deck.get(j);
            deck.set(i, randomCard);
            deck.set(j, currCard);
        }

        // Dát eso jako první kartu dealerovi
        /*for (int i = 0; i < deck.size(); i++) {
            Card card = deck.get(i);
            if (card.isAce()) {  // Předpokládáme, že eso má hodnotu "A"
                // Vyměňte eso s poslední kartou
                Card lastCard = deck.get(deck.size() - 1);
                deck.set(deck.size() - 1, card);
                deck.set(i, lastCard);
                break;
            }
        }*/ 

        // Dát hráčovi esa
        int count = 3;
        for (int i = 0; i < deck.size(); i++) {
            Card card = deck.get(i);
            if (card.isAce()) {  // Předpokládáme, že eso má hodnotu "A"
                // Vyměňte eso s poslední kartou
                Card playerCard = deck.get(deck.size() - count);
                deck.set(deck.size() - count, card);
                deck.set(i, playerCard);
                count++;
                if (count == 4) { //5: 2 esa, 6: 3 esa ...
                    break;
                }
            }
        }
    }
}

