import java.util.ArrayList;

public class Hand {
    ArrayList<Card> cards;

    public Hand() {
        cards = new ArrayList<>();
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public void removeCard(Card card) {
        cards.remove(card);
    }

    public Card getCard(int cardIndex) {
        return cards.get(cardIndex);
    }

    public int getSum() {
        int sum = 0;
        for (Card card : cards) {
            sum += card.getValue();
        }
        return sum;
    }
 
    public int size() {
        return cards.size();
    }

    public int getAceCount() {
        int count = 0;
        for (Card card : cards) {
            if (card.isAce()) {
                count++;
            }
        }
        return count;
    }

    static public int sumAfterAceEvaluation(int sum, int aceCount) {
        while (sum > 21 && aceCount > 0) {
            sum -= 10;
            aceCount -= 1;
        }
        return sum;
    }

    public boolean canSplit() {
        return cards.size() == 2 && cards.get(0).getValue() == cards.get(1).getValue();
    }

    @Override
    public String toString() {
        return cards.toString();
    }

}
