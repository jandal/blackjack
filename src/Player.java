import java.util.ArrayList;

public class Player {
    ArrayList<Hand> hands;
    int[] cardsSums;
    int[] aceCounts;
    int currentHandIndex;
    int money;
    int bet;
    String name;
    boolean playerLost;
    boolean playerLostThisRound;

    public Player(String name ,int startMoney) {
        hands = new ArrayList<>();
        hands.add(new Hand());  // přidá prázdnou ruku do playerHands
        this.money = startMoney; // inicializuje hračovo peníze a business :*
        this.name = name;
        this.playerLost = false;
        this.playerLostThisRound = false;
    }

    public void addHand() {
        hands.add(new Hand());  // přidá další ruku do playerHands
    }

    public Hand getCurrentHand() {
        return hands.get(currentHandIndex);  // Vrátí aktuální ruku
    }

    public void nextHand() {
        if (hasMoreHands()) {
            currentHandIndex++;  // Přejde na další ruku
        }
    }

    public boolean hasMoreHands() {
        return currentHandIndex < hands.size() - 1;  // Zkontroluje, zda existují další ruce k hraní
    }

    public boolean hasBlackjack() {
        return getCurrentHand().getSum() == 21;
    }
    
    public void placeBet(int betAmount) {
    	if(betAmount <= money) {
    		bet = betAmount;
    		money -= betAmount;
    	}
    }
    
    public void doubleDown() {
    	if(bet<=money) {
    		money -= bet;
    		bet*=2;
    	}
    }
    
    public void resetBet() {
    	bet = 0;
    }
    
}
