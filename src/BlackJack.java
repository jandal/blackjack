import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.*;
import javax.sound.sampled.*;

public class BlackJack {
    JFrame frame = new JFrame("BlackJack");
    Dealer dealer;
    ArrayList<Player> players = new ArrayList<>();
    Player currentPlayer;
    Deck deck;
    int currentPlayerIndex = 0;

    JPanel buttonPanel = new JPanel();
    JButton hitButton = new JButton("Hit");
    JButton stayButton = new JButton("Stay");
    JButton doubleDownButton = new JButton("Double Down");
    JButton splitButton = new JButton("Split");
    JButton replayButton = new JButton("Replay");
    JButton nextRoundButton = new JButton("Next Round");

    public BlackJack() {
        startGame();

        frame.setVisible(true);
        frame.setSize(GameWindow.boardWidth + 200, GameWindow.boardHeight); 
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        gamePanel.setLayout(new BorderLayout());
        gamePanel.setBackground(new Color(40, 100, 80));
        frame.add(gamePanel);

        hitButton.setFocusable(false);
        buttonPanel.add(hitButton);

        stayButton.setFocusable(false);
        buttonPanel.add(stayButton);

        doubleDownButton.setFocusable(false);
        buttonPanel.add(doubleDownButton);

        splitButton.setFocusable(false);
        buttonPanel.add(splitButton);

        nextRoundButton.setFocusable(false);
        buttonPanel.add(nextRoundButton);
        nextRoundButton.setEnabled(false);

        replayButton.setFocusable(false);
        buttonPanel.add(replayButton);
        frame.add(buttonPanel, BorderLayout.SOUTH);

        // Action listener pro hit
        hitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Clip hitClip = null;
                try {
                    hitClip = loadSound("./sfx/card-flip.wav");
                } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
                    throw new RuntimeException(ex);
                }
                playClip(hitClip);

                Card card = deck.getCard();
                currentPlayer.cardsSums[currentPlayer.currentHandIndex] += card.getValue();
                currentPlayer.aceCounts[currentPlayer.currentHandIndex] += card.isAce() ? 1 : 0;
                currentPlayer.getCurrentHand().addCard(card);
                doubleDownButton.setEnabled(false);

                if (Hand.sumAfterAceEvaluation(currentPlayer.cardsSums[currentPlayer.currentHandIndex], currentPlayer.aceCounts[currentPlayer.currentHandIndex]) > 21 && !currentPlayer.hasMoreHands()) {
                    currentPlayer.nextHand();
                    if (!currentPlayer.hasMoreHands()) {
                        nextPlayerTurn();
                    }
                } else if (Hand.sumAfterAceEvaluation(currentPlayer.cardsSums[currentPlayer.currentHandIndex], currentPlayer.aceCounts[currentPlayer.currentHandIndex]) > 21 && currentPlayer.hasMoreHands()) {
                    currentPlayer.nextHand();
                }

                gamePanel.repaint();
            }
        });

        // Action listener pro stay
        stayButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Clip stayClip = null;
                try {
                    stayClip = loadSound("/sfx/card-flip.wav"); 
                } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
                    throw new RuntimeException(ex);
                }
                playClip(stayClip);

                if (currentPlayer.hasMoreHands()) {
                    currentPlayer.nextHand();
                } else {
                    nextPlayerTurn();
                }
                gamePanel.repaint();
            }
        });

        // Action listener pro doubledown
        doubleDownButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Clip dbClip = null;
                try {
                    dbClip = loadSound("/sfx/card-flip.wav"); 
                } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
                    throw new RuntimeException(ex);
                }
                playClip(dbClip);
                currentPlayer.doubleDown();
                hitButton.setEnabled(false);
                stayButton.setEnabled(false);
                doubleDownButton.setEnabled(false);
                splitButton.setEnabled(false);

                Card card = deck.getCard();
                currentPlayer.cardsSums[0] += card.getValue();
                currentPlayer.aceCounts[0] += card.isAce() ? 1 : 0;
                currentPlayer.getCurrentHand().addCard(card);
                Hand.sumAfterAceEvaluation(currentPlayer.cardsSums[0], currentPlayer.aceCounts[0]);

                nextPlayerTurn();
                gamePanel.repaint();
            }
        });

        // Action listener pro split
        splitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Clip splitClip = null;
                try {
                    splitClip = loadSound("/sfx/card-flip.wav"); 
                } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
                    throw new RuntimeException(ex);
                }
                playClip(splitClip);
                Hand currentHand = currentPlayer.getCurrentHand();
                if (currentHand.canSplit()) {
                    currentPlayer.addHand();
                    Hand newHand = currentPlayer.hands.get(currentPlayer.hands.size() - 1);
                    Card cardToMove = currentHand.getCard(1);
                    currentHand.removeCard(cardToMove);
                    newHand.addCard(cardToMove);

                    currentPlayer.cardsSums = new int[currentPlayer.hands.size()];
                    currentPlayer.aceCounts = new int[currentPlayer.hands.size()];
                    for (int i = 0; i < currentPlayer.hands.size(); i++) {
                        currentPlayer.cardsSums[i] = currentPlayer.hands.get(i).getSum();
                        currentPlayer.aceCounts[i] = currentPlayer.hands.get(i).getAceCount();
                    }

                    hitButton.setEnabled(true);
                    stayButton.setEnabled(true);
                    doubleDownButton.setEnabled(true);
                    splitButton.setEnabled(currentPlayer.getCurrentHand().canSplit());

                    gamePanel.repaint();
                }
            }
        });

        // Action listener pro replay
        replayButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new BlackJack();
            }
        });

        // Action listener pro next
        nextRoundButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                nextRound();
            }
        });

        gamePanel.repaint();
    }

    // inicializace
    public void startGame() {
        gamePanel.repaint();
        dealer = new Dealer();
        deck = new Deck();
        int numPlayers = 0;

        // kolik hracu bude hrat
        while (numPlayers < 1 || numPlayers > 4) {
            String numPlayersString = JOptionPane.showInputDialog("Enter number of players (1-4):");
            numPlayers = Integer.parseInt(numPlayersString);
        }

        players.clear(); 
        for (int i = 0; i < numPlayers; i++) {
            String playerName = JOptionPane.showInputDialog("Enter name for player " + (i + 1) + ":");
            int initialMoney = 0;
            while (initialMoney <= 0) {
                String initialMoneyString = JOptionPane.showInputDialog(playerName + ", how much money are you playing with?");
                try {
                    initialMoney = Integer.parseInt(initialMoneyString);
                } catch (Exception e) {
                    initialMoney = 0;
                }
            }
            
            Player player = new Player(playerName, initialMoney);
            players.add(player);
        }
        
        placeBets();
        dealInitialCards();
        doubleDownButton.setEnabled(currentPlayer.money >= currentPlayer.bet);
        Clip startClip = null;
        try {
            startClip = loadSound("/sfx/lick_tenor_sax.wav"); 
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
            throw new RuntimeException(ex);
        }
        playClip(startClip);
    }

    // dotaz na vsechny hrace, aby polozili sazku
    public void placeBets() {
        for (Player player : players) {
            if (player.money > 0 && !player.playerLost) {
                int betAmount = 0;
                while (betAmount <= 0 || betAmount > player.money) {
                    String betString = JOptionPane.showInputDialog(player.name + ", place your bet:");
                    try {
                        betAmount = Integer.parseInt(betString);
                    } catch (Exception e) {
                        betAmount = 0;
                    }
                    if (betAmount > player.money) {
                        JOptionPane.showMessageDialog(null, "Bet amount cannot exceed available money.");
                    }
                }
                player.placeBet(betAmount);
            }
        }
    }

    // puvodni rozdani karet
    public void dealInitialCards() {
        dealer.hiddenCard = deck.getCard();
        dealer.hand.addCard(dealer.hiddenCard);
        dealer.aceCount += dealer.hiddenCard.isAce() ? 1 : 0;

        Card card = deck.getCard();
        dealer.hand.addCard(card);
        dealer.aceCount += card.isAce() ? 1 : 0;
        dealer.handSum = dealer.hand.getSum();

        for (Player player : players) {
            if (!player.playerLost) { 
                player.hands = new ArrayList<>();
                player.hands.add(new Hand());
                player.cardsSums = new int[1];
                player.aceCounts = new int[1];

                for (int i = 0; i < 2; i++) {
                    card = deck.getCard();
                    player.aceCounts[0] += card.isAce() ? 1 : 0;
                    player.getCurrentHand().addCard(card);
                }

                player.cardsSums[0] = player.getCurrentHand().getSum();
            }
        }

        currentPlayerIndex = 0;
        while (currentPlayerIndex < players.size() && players.get(currentPlayerIndex).playerLost) {
            currentPlayerIndex++;
        }
        if (currentPlayerIndex < players.size()) {
            currentPlayer = players.get(currentPlayerIndex);
        }
        gamePanel.repaint();
    }

    // postupne jdou na radu vsichni hraci
    public void nextPlayerTurn() {
        currentPlayerIndex++;
        while (currentPlayerIndex < players.size()) {
            currentPlayer = players.get(currentPlayerIndex);
            if (!currentPlayer.playerLost) {
                hitButton.setEnabled(true);
                stayButton.setEnabled(true);
                doubleDownButton.setEnabled(currentPlayer.money >= currentPlayer.bet); 
                splitButton.setEnabled(currentPlayer.getCurrentHand().canSplit());
                return; 
            } else {
                currentPlayerIndex++;
            }
        }
        dealerTurn(); 
    }

    // logika pro radu dealera
    public void dealerTurn() {
        while (dealer.handSum < 17) {
            Card card = deck.getCard();
            dealer.handSum += card.getValue();
            dealer.aceCount += card.isAce() ? 1 : 0;
            dealer.hand.addCard(card);
            dealer.handSum = Hand.sumAfterAceEvaluation(dealer.handSum, dealer.aceCount);
        }

        determineWinners();
        hitButton.setEnabled(false);
        stayButton.setEnabled(false);
        doubleDownButton.setEnabled(false);
        splitButton.setEnabled(false);
        nextRoundButton.setEnabled(true);
        gamePanel.repaint();
    }

    // rozhodne konec kola, rozdeli sazky
    public void determineWinners() {
        boolean allPlayersBroke = true;

        for (Player player : players) {
            if (!player.playerLost) {
                for (int i = 0; i <= player.currentHandIndex; i++) {
                    int playerSum = Hand.sumAfterAceEvaluation(player.cardsSums[i], player.aceCounts[i]);
                    int dealerSum = dealer.handSum;

                    if (playerSum > 21) {
                        // Player loses
                    } else if (dealerSum > 21 || playerSum > dealerSum) {
                        player.money += 2 * player.bet; 
                    } else if (playerSum == dealerSum) {
                        player.money += player.bet; 
                    }
                    
                    if (player.money == 0) {
                        player.playerLostThisRound = true;
                        player.playerLost = true;
                        player.hands.clear();
                        player.hands.add(new Hand());
                        nextRoundButton.setEnabled(false);
                        replayButton.setEnabled(true);
                    }
                }
                player.resetBet();
            }
            if (player.money <= 0 && !player.playerLost) {
                player.money = 0;
                player.playerLostThisRound = true; 
                player.playerLost = true;
                nextRoundButton.setEnabled(false);
                replayButton.setEnabled(true);
            } else if (!player.playerLost) {
                allPlayersBroke = false;
            }
        }

        Clip startClip = null;
        try {
            startClip = loadSound("/sfx/goofy-ahh-ringtone.wav");
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
            throw new RuntimeException(ex);
        }
        playClip(startClip);
        
        if (allPlayersBroke) {
            JOptionPane.showMessageDialog(null, "All player lost, please restart.");
            nextRoundButton.setEnabled(false);
            replayButton.setEnabled(true);
        } else {
            //nextRoundButton.setEnabled(true);
        }
    }

    // dalsi kolo, reloaduje potrebne komponenty
    public void nextRound() {
        nextRoundButton.setEnabled(false);
        for (Player player : players) {
            if (player.money > 0 && !player.playerLost) { 
                player.hands.clear();
                player.hands.add(new Hand());
                player.currentHandIndex = 0;
                player.cardsSums = new int[1];
                player.aceCounts = new int[1];
                player.playerLostThisRound = false; 
            }
        }
        currentPlayerIndex = 0;
        dealer = new Dealer();
        deck = new Deck();
        placeBets();
        dealInitialCards();
        hitButton.setEnabled(true);
        stayButton.setEnabled(true);
        doubleDownButton.setEnabled(currentPlayer.money >= currentPlayer.bet);
        splitButton.setEnabled(true);
        gamePanel.repaint();

        Clip nextClip = null;
        try {
            nextClip = loadSound("/sfx/card-sound.wav"); 
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
            throw new RuntimeException(ex);
        }
        playClip(nextClip);
    }

    // nahraje zvuk ze souboru
    public static Clip loadSound(String path) throws IOException, LineUnavailableException, UnsupportedAudioFileException {
        URL audioUrl = BlackJack.class.getResource(path);
        AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioUrl);
        AudioFormat format = audioStream.getFormat();
        DataLine.Info info = new DataLine.Info(Clip.class, format);

        Clip clip = (Clip) AudioSystem.getLine(info);
        clip.open(audioStream); 
        System.out.println("Loading sound: " + path);

        FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
        gainControl.setValue(-10.0f); 

        return clip;
    }

    // metoda na hrani zvuku
    public static void playClip(Clip clip) {
        if (clip == null || !clip.isOpen()) {
            return; 
        }
        if (clip.isRunning()) {
            clip.stop(); 
        }
        clip.setFramePosition(0); 
        clip.start();
    }

    // rendering
    JPanel gamePanel = new JPanel() {
        private static final long serialVersionUID = 1L;

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            int verticalSpacing = 20;
            int playerRowSpacing = 375;

            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));

            try {
                for (int playerIndex = 0; playerIndex < players.size(); playerIndex++) {
                    Player player = players.get(playerIndex);
                    for (int handIndex = 0; handIndex < player.hands.size(); handIndex++) {
                        Hand hand = player.hands.get(handIndex);
                        int baseX;
                        int baseY;

                        if (playerIndex < 2) {
                            baseX = GameWindow.cardX + playerIndex * 500;
                            baseY = GameWindow.cardY + 300;
                        } else {
                            baseX = GameWindow.cardX + (playerIndex - 2) * 500;
                            baseY = GameWindow.cardY + 300 + playerRowSpacing;
                        }

                        if (playerIndex == currentPlayerIndex && hand == player.getCurrentHand()) {
                            g2d.setColor(Color.WHITE);
                            g2d.fillRect(baseX - 5, baseY - 5, (GameWindow.cardWidth + 5) * hand.size() + 10, GameWindow.cardHeight + 10);
                        }
                    }
                }

                g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Image hiddenCardImg = new ImageIcon(getClass().getResource("./cards/BACK.png")).getImage();
                if (!stayButton.isEnabled() && !hitButton.isEnabled()) {
                    hiddenCardImg = new ImageIcon(getClass().getResource(dealer.hiddenCard.getImagePath())).getImage();
                }
                g.drawImage(hiddenCardImg, GameWindow.cardX, GameWindow.cardY, GameWindow.cardWidth, GameWindow.cardHeight, null);

                for (int i = 1; i < dealer.hand.cards.size(); i++) {
                    Card card = dealer.hand.cards.get(i);
                    Image cardImg = new ImageIcon(getClass().getResource(card.getImagePath())).getImage();
                    g.drawImage(cardImg, GameWindow.cardWidth + GameWindow.cardX + 5 + (GameWindow.cardWidth + 5) * (i - 1), GameWindow.cardY, GameWindow.cardWidth, GameWindow.cardHeight, null);
                }

                for (int playerIndex = 0; playerIndex < players.size(); playerIndex++) {
                    Player player = players.get(playerIndex);
                    int baseX;
                    int baseY;

                    if (playerIndex < 2) {
                        baseX = GameWindow.cardX + playerIndex * 500; 
                        baseY = GameWindow.cardY + 300;
                    } else {
                        baseX = GameWindow.cardX + (playerIndex - 2) * 500;
                        baseY = GameWindow.cardY + 300 + playerRowSpacing;
                    }

                    g.setFont(new Font("Arial", Font.PLAIN, 20));
                    g.setColor(Color.WHITE);
                    g.drawString(player.name + ": $" + player.money, baseX, baseY - 20);

                    for (int handIndex = 0; handIndex < player.hands.size(); handIndex++) {
                        Hand hand = player.hands.get(handIndex);
                        for (int cardIndex = 0; cardIndex < hand.size(); cardIndex++) {
                            Card card = hand.getCard(cardIndex);
                            Image cardImg = new ImageIcon(getClass().getResource(card.getImagePath())).getImage();
                            int x = baseX + (GameWindow.cardWidth + 5) * cardIndex;
                            int y = baseY + (GameWindow.cardHeight + verticalSpacing) * handIndex;
                            g.drawImage(cardImg, x, y, GameWindow.cardWidth, GameWindow.cardHeight, null);
                        }
                    }
                }

                if (currentPlayer.getCurrentHand().canSplit()) {
                    splitButton.setEnabled(true);
                } else {
                    splitButton.setEnabled(false);
                }

                if (!stayButton.isEnabled() && !hitButton.isEnabled()) {
                    dealer.handSum = Hand.sumAfterAceEvaluation(dealer.handSum, dealer.aceCount);
                    for (Player player : players) {
                        for (int i = 0; i < player.hands.size(); i++) {
                            player.cardsSums[i] = Hand.sumAfterAceEvaluation(player.cardsSums[i], player.aceCounts[i]);
                        }
                    }
                    String message = "";
                    for (int playerIndex = 0; playerIndex < players.size(); playerIndex++) {
                        Player player = players.get(playerIndex);
                        int messageY = GameWindow.cardY + 10 + playerIndex * 80;
                        for (int i = 0; i <= player.currentHandIndex; i++) {
                            if (player.cardsSums[i] > 21) {
                                message = "Player " + (playerIndex + 1) + " Hand " + (i + 1) + ": You Lose!";
                            } else if (dealer.handSum > 21) {
                                message = "Player " + (playerIndex + 1) + " Hand " + (i + 1) + ": You Win!";
                            } else if (player.cardsSums[i] == dealer.handSum) {
                                message = "Player " + (playerIndex + 1) + " Hand " + (i + 1) + ": Push!";
                            } else if (player.cardsSums[i] > dealer.handSum) {
                                message = "Player " + (playerIndex + 1) + " Hand " + (i + 1) + ": You Win!";
                            } else {
                                message = "Player " + (playerIndex + 1) + " Hand " + (i + 1) + ": You Lose!";
                            }
                            g.setFont(new Font("Arial", Font.PLAIN, 30));
                            g.setColor(Color.white);
                            g.drawString(message, 700, messageY + (i * 30));
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
